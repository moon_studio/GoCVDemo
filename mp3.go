package main

import (
	"github.com/hajimehoshi/oto"
	"github.com/tosone/minimp3"
	"io/ioutil"
	"time"
)

func main() {
	var file, _ = ioutil.ReadFile("data/aigei.mp3.mp3")
	dec, data, _ := minimp3.DecodeFull(file)

	player, _ := oto.NewPlayer(dec.SampleRate, dec.Channels, 2, 1024)
	player.Write(data)
	<-time.After(time.Second)
	dec.Close()
	player.Close()
	//var err error
	//
	//var dec *minimp3.Decoder
	//var data, file []byte
	//var player *oto.Player
	//
	//if file, err = ioutil.ReadFile("data/aigei.mp3"); err != nil {
	//	log.Fatal(err)
	//}
	//
	//if dec, data, err = minimp3.DecodeFull(file); err != nil {
	//	log.Fatal(err)
	//}
	//
	//if player, err = oto.NewPlayer(dec.SampleRate, dec.Channels, 2, 1024); err != nil {
	//	log.Fatal(err)
	//}
	//player.Write(data)
	//
	//<-time.After(time.Second)
	//dec.Close()
	//player.Close()
}
